using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "normalUser")]
    public class CashFlowController : Controller
    {
        
        
        // GET
        public ActionResult Index(int? page)
        {
        
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;
            CashFlowDao cashFlowD = new CashFlowDao();
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<CashFlow> cashFlows = cashFlowD.GetCashFlowPaged(itemsOnPage, pg, out totalBooks,user.Id);
     
            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;
            ViewBag.EaCategories = new EarningCategoryDao().GetAll();
            ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView(cashFlows);
            }
          
            return View(cashFlows);
        }
        
   
        public ActionResult Create()
        {
            
            ExpenditureCategoryDao expenditureCategory= new ExpenditureCategoryDao();
            EarningCategoryDao earningCategoryDao = new EarningCategoryDao();
            IList<ExpenditureCategory> categories = expenditureCategory.GetAll();
            IList<EarningCategory> earningCategories = earningCategoryDao.GetAll();
            
            ViewBag.EaCategories = new EarningCategoryDao().GetAll();
            ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();
            
            return View();
        }

        public ActionResult Add(CashFlow cashFlow,int? eaCategoryId,int? exCategoryId)
        {
           ViewBag.EaCategories = new EarningCategoryDao().GetAll();
           ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();         
           if (ModelState.IsValid)
            {
                if (cashFlow.Earning)
                {
                    EarningCategory earningCategory = new EarningCategoryDao().GetById(eaCategoryId);
                    cashFlow.CategoryOfItem = earningCategory;
                }
                else
                {
                    ExpenditureCategory expenditureCategory = new ExpenditureCategoryDao().GetById(exCategoryId);
                    
                    cashFlow.CategoryOfItem1 = expenditureCategory;
                }
                if (User.Identity.IsAuthenticated)
                {
                    cashFlow.UserId = new UserDao().GetByLogin(User.Identity.Name);                   
                }
                   CashFlowDao cashFlowDao = new CashFlowDao();
                    cashFlowDao.Create(cashFlow);


                    if (cashFlow.Earning)
                    {
                         TempData["message-success"] = "Příjem "+ cashFlow.Note +" úspěšně přidán";
                    }
                    else
                    {
                        TempData["message-success"] = "Výdaj "+ cashFlow.Note  +" úspěšně přidán"; 
                    }
                   
                
                 
                
            }
            else
            {
                return View("Create",cashFlow);
            }

            return RedirectToAction("Index");
            
            
            
            
        }
        
        public ActionResult Edit(int id)
        {
            CashFlowDao cashFlowDao = new CashFlowDao();
            CashFlow c = cashFlowDao.GetById(id);
            if (c.CategoryOfItem == null)
            {
                c.CategoryOfItem =  new EarningCategoryDao().GetById(1);
            }
            if (c.CategoryOfItem1 == null)
            {
                c.CategoryOfItem1 = new ExpenditureCategoryDao().GetById(1);
            }
            ViewBag.EaCategories = new EarningCategoryDao().GetAll();
            ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();
            
           
            return View(c);

        }


       
         public ActionResult Update(CashFlow c,int? eaCategoryId,int? exCategoryId)
                {
                    

                    if (c.Earning)
                    {
                        EarningCategory earningCategory = new EarningCategoryDao().GetById(eaCategoryId);
                        c.CategoryOfItem = earningCategory;
                    }
                    else
                    {
                        ExpenditureCategory expenditureCategory = new ExpenditureCategoryDao().GetById(exCategoryId);
                        c.CategoryOfItem1 = expenditureCategory;
                    }
                    
                   
                    try
                    {
                        CashFlowDao cashFlowDao = new CashFlowDao();
                       

                       
                        
                        cashFlowDao.Update(c);
        
                        if (c.Earning)
                        {
                            TempData["message-success"] = "Příjem "+ c.Note +" úspěšně upraven";
                        }
                        else
                        {
                            TempData["message-success"] = "Výdaj "+ c.Note  +" úspěšně upraven"; 
                        }
                    }
                    catch (Exception e)
                    {
                        TempData["message-failed"] = "Něco se pokazilo :(";
                        return RedirectToAction("Index", "CashFlow");
                    }
                   
                    return RedirectToAction("Index", "CashFlow");
        
                }

       
         public ActionResult Delete(int id)
         {
            try
             {
                 CashFlowDao cashFlowDao = new CashFlowDao();
                 CashFlow cashFlow = cashFlowDao.GetById(id);
               
            
                 cashFlowDao.Delete(cashFlow);

                 if (cashFlow.Earning)
                 {
                     TempData["message-success"] = "Příjem "+ cashFlow.Note +" úspěšně smazán";
                 }
                 else
                 {
                     TempData["message-success"] = "Výdaj "+ cashFlow.Note  +" úspěšně smazán"; 
                 }
             }
             catch (Exception e)
             {
               
                 TempData["message-failed"] = "Něco se pokazilo :(";
                 return RedirectToAction("Index", "CashFlow");
             }

             return RedirectToAction("Index");
         }
         
         
         [HttpPost]
         public ActionResult Search(string phrase, int? page)
         {
             try
             {
                 int itemsOnPage = 10;
                 int pg = page.HasValue ? page.Value : 1;
                 User user = new UserDao().GetByLogin(User.Identity.Name);
                 CashFlowDao cashFlowDao = new CashFlowDao();
                 IList<CashFlow> cashFlows = cashFlowDao.SearchCashFlows(phrase, itemsOnPage, pg,user.Id);

                 int totalBooks = cashFlows.Count;

                 ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                 ViewBag.CurrentPage = pg;

                 ViewBag.Categories = new DocumentCategoryDao().GetAll();

                 if (Request.IsAjaxRequest())
                 {
                     return PartialView("Index", cashFlows);
                 }

                 return View("Index", cashFlows);

             }
             catch (Exception e)
             {
                 
                        TempData["message-failed"] = "Něco se pokazilo :(";
                        return RedirectToAction("Index", "CashFlow");
             }
            
         }
         
        
         public ActionResult EarningCategory(int id,int? page)
         {
             try
             {
                 int itemsOnPage = 10;
                 int pg = page.HasValue ? page.Value : 1;
                 User user = new UserDao().GetByLogin(User.Identity.Name);
                 IList<CashFlow> cashFlows= new CashFlowDao().GetCashFlowByEaCategoryId(id,itemsOnPage,pg,user.Id);
            
                 int totalBooks = cashFlows.Count;
            
                 ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                 ViewBag.CurrentPage = pg;

                 ViewBag.EaCategories = new EarningCategoryDao().GetAll();
                 ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();
            
            
                 if (Request.IsAjaxRequest())
                 {
                     return PartialView("Index", cashFlows);
                 }

                 return View("Index", cashFlows);
             }
             catch (Exception e)
             {
                    TempData["message-failed"] = "Něco se pokazilo :(";
                                         return RedirectToAction("Index", "CashFlow");
             }
            
         }
         
      
         public ActionResult ExpenditureCategory(int id,int? page)
         {
             try
             {
                 int itemsOnPage = 10;
                 int pg = page.HasValue ? page.Value : 1;
                 User user = new UserDao().GetByLogin(User.Identity.Name);
                 IList<CashFlow> cashFlows= new CashFlowDao().GetCashFlowByExCategoryId(id,itemsOnPage,pg,user.Id);
            
                 int totalBooks = cashFlows.Count;
            
                 ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                 ViewBag.CurrentPage = pg;

                 ViewBag.EaCategories = new EarningCategoryDao().GetAll();
                 ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();
            
            
                 if (Request.IsAjaxRequest())
                 {
                     return PartialView("Index", cashFlows);
                 }

                 return View("Index", cashFlows);
             }
             catch (Exception e)
             {
                  TempData["message-failed"] = "Něco se pokazilo :(";
                  return RedirectToAction("Index", "CashFlow");
             }
            
         }
         
       
         public ActionResult FilterByDate(DateTime startDate, DateTime? endDate, int? page)
         {
             try
             {
                 int itemsOnPage = 10;
                 int pg = page.HasValue ? page.Value : 1;


                 if (endDate == null)
                 {
                     endDate = DateTime.Today;
                 }
                 User user = new UserDao().GetByLogin(User.Identity.Name);
                 IList<CashFlow> cashFlows =
                     new CashFlowDao().GetCasFlowFilteredByDate(startDate, endDate, itemsOnPage, pg,user.Id);

                 int totalBooks = cashFlows.Count;

                 ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                 ViewBag.CurrentPage = pg;

                 ViewBag.EaCategories = new EarningCategoryDao().GetAll();
                 ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();

                 if (Request.IsAjaxRequest())
                 {
                     return PartialView("Index", cashFlows);
                 }

                 return View("Index", cashFlows);
             }
             catch (Exception e)
             {
                 TempData["message-failed"] = "Něco se pokazilo :(";
                 return RedirectToAction("Index", "CashFlow");

             }
            
         }
         
         [HttpPost]
         public ActionResult FilterByType(bool earning, int? page)
         {
             try
             {
                 int itemsOnPage = 10;
                 int pg = page.HasValue ? page.Value : 1;
                 User user = new UserDao().GetByLogin(User.Identity.Name);
                 IList<CashFlow> cashFlows =
                     new CashFlowDao().SelectOnlyX(earning, itemsOnPage, pg,user.Id);

                 int totalBooks = cashFlows.Count;

                 ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                 ViewBag.CurrentPage = pg;

                 ViewBag.EaCategories = new EarningCategoryDao().GetAll();
                 ViewBag.ExCategories = new ExpenditureCategoryDao().GetAll();

                 if (Request.IsAjaxRequest())
                 {
                     return PartialView("Index", cashFlows);
                 }

                 return View("Index", cashFlows);
             }
             catch (Exception e)
             {
                 TempData["message-failed"] = "Něco se pokazilo :(";
                 return RedirectToAction("Index", "CashFlow");
             }
            
         }

        
         
    }
}