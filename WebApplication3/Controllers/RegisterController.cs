
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    public class RegisterController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return
            View();
        }

   
        public ActionResult SignIn(string login, string password, string firstName, string lastName,
            string emailOfUser)
        {
            
            UserDao userDao = new UserDao();
          
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            Byte [] hashedPassword = sha256.ComputeHash(GetBytesForPassword(password));
            password = Encoding.ASCII.GetString(hashedPassword);
            
            
            User user = new   User(firstName,lastName,login,emailOfUser,password,Role.SetDefaultRol());
            if (userDao.isLoginFree(login))
            {
                try
                {
                    userDao.Create(user);
                    return RedirectToAction("Index", "Login");
                }
                catch (Exception e)
                {TempData["message-failed"] = "Něco se pokazilo :( ";
                              return RedirectToAction("Index", "Register");
                }
             
               
            }
            
                TempData["message-failed"] = "Vámi zvolený login je již zabrán zkuste prosím jiný";
                return View("Index", user);

            
            
                
                
            
        }

        private byte[] GetBytesForPassword(string password)
        {
            return Encoding.ASCII.GetBytes(password);
        }
    }
}