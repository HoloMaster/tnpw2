using System.Collections.Generic;

namespace DataContainer.Interface
{
    public interface IDaoBase<E>
    {
        IList<E> GetAll();

        object Create(E enity);

        void Update(E entity);

        void Delete(E entity);

        E GetById(int? id);
        
        


    }
}