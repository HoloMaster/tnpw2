﻿using System.Web.Mvc;

namespace WebApplication3.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Home", action = "AdminIndex", id = UrlParameter.Optional },
                namespaces: new []{"WebApplication3.Areas.Admin.Controllers"}
                
            );
        }
    }
}