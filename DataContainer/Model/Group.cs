using System;
using System.ComponentModel.DataAnnotations;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class Group : IEntity
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Název skupiny je vyžadován")]
        public virtual String Name { get; set; }
        
        public virtual String Note { get; set; }
    }
}