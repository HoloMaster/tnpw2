using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "normalUser")]
    public class UserController : Controller
    {
        // GET
        public ActionResult Index()
        {
           
           UserDao userDao = new UserDao();
           User user = userDao.GetByLogin(User.Identity.Name);
           GroupUserDao groupUserDao = new GroupUserDao();
           ViewBag.Groups = groupUserDao.GetGroupWhereUser(user);
           
                
            return View(user);
        }

        public ActionResult Add(User user)
        {
            return View();
            
        }

       
        public ActionResult Edit(int id)
        {
            UserDao ud = new UserDao();
            User user = ud.GetById(id);
            
            return PartialView(user);
            
        }

        
        public ActionResult Update(User u)
        {
            try
            {
                UserDao ud = new UserDao(); 
                
                ud.Update(u);
        
                 
                TempData["message-success"] = "Uživatel "+ u.FirstName +" "+ u.LastName+" úspěšně vytvořena";

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
                   
            return RedirectToAction("Index", "User");
        
        }
        }



    }
