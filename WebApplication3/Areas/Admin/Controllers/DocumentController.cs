﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using DataContainer.Dao;
using DataContainer.Model;
using WebApplication3.Helpers;
using WebApplication3.utils;

namespace WebApplication3.Areas.Admin.Controllers
{
    [Authorize]
    public class DocumentController : Controller
    {
        
        // GET: Document
        public ActionResult Index(int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            DocumentDao documentDao = new DocumentDao();
            IList<Document> documents = documentDao.GetDocumentPaged(itemsOnPage, pg, out totalBooks,user.Id);

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView(documents);
            }

            return View(documents);
        }

        [HttpPost]
        public ActionResult Detail(int id)
        {
            DocumentDao documentDao = new DocumentDao();
            Document d = documentDao.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(d);
            }

            return View(d);
        }

        [HttpPost]
        public ActionResult Category(int id,int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<Document> documents = new DocumentDao().GetDocumentInCategoryId(id,itemsOnPage,pg,user.Id);
            
            int totalBooks = documents.Count;
            
            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();
            
            
            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", documents);
            }

            return View("Index", documents);
        }

        public ActionResult FilterByDate(DateTime startDate, DateTime endDate, int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<Document> documents =
                new DocumentDao().GetDocumentFilteredByDate(startDate, endDate, itemsOnPage, pg,user.Id);

            int totalBooks = documents.Count;

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", documents);
            }

            return View("Index", documents);
        }

        [HttpPost]
        public ActionResult Search(string phrase, int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            DocumentDao documentDao = new DocumentDao();
            IList<Document> documents = documentDao.SearchBooks(phrase, itemsOnPage, pg,user.Id);
            
            int totalBooks = documents.Count;

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", documents);
            }

            return View("Index", documents);
        }

        [HttpPost]
        public JsonResult SearchWithHelp(string query)
        {
            DocumentDao documentDao = new DocumentDao();
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<Document> documents = documentDao.SearchBooks(query,user.Id);
            List<string> names = (from Document d in documents select d.Name).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create()
        {
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
            IList<DocumentCategory> documentCategories = documentCategoryDao.GetAll();
            ViewBag.DocumentCategories = documentCategories;
            return View();
        }

        [HttpPost]
        public ActionResult Add(Document document, HttpPostedFileBase image, int documentId)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    if (image.ContentType == "image/png" ||
                        image.ContentType == "image/jpg" || image.ContentType == "image/jpeg")
                    {
                        Image scan = Image.FromStream(image.InputStream);
                        Bitmap b = new Bitmap(scan);

                      
                        Guid guid = Guid.NewGuid();
                        string imageName = null;
                        if (image.ContentType == "image/png")
                        {
                            imageName = guid.ToString() + ".png";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Png);
                        }

                        if (image.ContentType == "image/jpg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }

                        if (image.ContentType == "image/jpeg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }
                        
                       
                        scan.Dispose();
                        b.Dispose();

                        document.Type = imageName;
                    }

                }

                document.DateOfEstablishment = DateTime.Today.Date;
                document.DocumentCategory = new DocumentCategoryDao().GetById(documentId);
                document.UserId = new UserDao().GetByLogin(User.Identity.Name);
                DocumentDao documentDao = new DocumentDao();
                documentDao.Create(document);
                
                for (int i = 1; i < 12 ; i = i +12/document.FrequencyOfPaymentPerYear)
                {
                    
                    Notification notification = new Notification();
                    notification.User = new UserDao().GetByLogin(User.Identity.Name);
                    notification.DateOfPay = document.DueDate.AddMonths(i-1);
                    notification.Document = document;
                    notification.Note = document.Name;
                    NotificationDao notificationDao = new NotificationDao();
                    notificationDao.Create(notification);  
                }
               
                
                
                
                TempData["message-success"] = "Dokument" + document.Name + "pridan";
            }
            else
            {
                
                return View("Create", document);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(int id)
        {
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
            IList<DocumentCategory> documentCategories = documentCategoryDao.GetAll();
            ViewBag.DocumentCategories = documentCategories;

            DocumentDao documentDao = new DocumentDao();
            Document d = documentDao.GetById(id);
            return View(d);
        }

        [HttpPost]
        public ActionResult Update(Document d, HttpPostedFileBase picture, int documentId)
        {
            try
            {
                DocumentDao documentDao = new DocumentDao();
                d.DocumentCategory = new DocumentCategoryDao().GetById(documentId);
                documentDao.Update(d);
                TempData["message-success"] = "Dokument " + d.Name + " byla upravena";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index", "Document");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                DocumentDao documentDao = new DocumentDao();
                Document document = documentDao.GetById(id);
                documentDao.DeleteNotificaitionFromDocuments(id);
                System.IO.File.Delete(Server.MapPath("~/uploads/invoices/" + document.Type));

                documentDao.Delete(document);

                TempData["message-success"] = "Dokument "+ document.Name +" byl úspěšně smazán";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index");
        }
    }
}