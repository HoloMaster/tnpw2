USE [master]
GO
/****** Object:  Database [SpravaFinanci]    Script Date: 15. 4. 2019 14:10:54 ******/
CREATE DATABASE [SpravaFinanci]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SpravaFinanci', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\SpravaFinanci.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SpravaFinanci_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\SpravaFinanci_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SpravaFinanci] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SpravaFinanci].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SpravaFinanci] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SpravaFinanci] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SpravaFinanci] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SpravaFinanci] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SpravaFinanci] SET ARITHABORT OFF 
GO
ALTER DATABASE [SpravaFinanci] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SpravaFinanci] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SpravaFinanci] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SpravaFinanci] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SpravaFinanci] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SpravaFinanci] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SpravaFinanci] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SpravaFinanci] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SpravaFinanci] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SpravaFinanci] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SpravaFinanci] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SpravaFinanci] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SpravaFinanci] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SpravaFinanci] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SpravaFinanci] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SpravaFinanci] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SpravaFinanci] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SpravaFinanci] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SpravaFinanci] SET  MULTI_USER 
GO
ALTER DATABASE [SpravaFinanci] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SpravaFinanci] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SpravaFinanci] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SpravaFinanci] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SpravaFinanci] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SpravaFinanci] SET QUERY_STORE = OFF
GO
USE [SpravaFinanci]
GO
/****** Object:  User [jan]    Script Date: 15. 4. 2019 14:10:54 ******/
CREATE USER [jan] FOR LOGIN [jan] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [jan]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [jan]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [jan]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [jan]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [jan]
GO
ALTER ROLE [db_datareader] ADD MEMBER [jan]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [jan]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [jan]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [jan]
GO
/****** Object:  Table [dbo].[tblCashFlow]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCashFlow](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[eaCategory_id] [int] NULL,
	[dateTime] [date] NULL,
	[note] [varchar](50) NULL,
	[user_id] [int] NULL,
	[amount] [varchar](50) NULL,
	[earning] [bit] NULL,
	[exCategory_id] [int] NULL,
 CONSTRAINT [PK_tblCashFlow] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCategory]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category] [varchar](50) NULL,
 CONSTRAINT [PK_tblCategory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDocument]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDocument](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dateOfEsteblishment] [date] NULL,
	[name] [varchar](50) NULL,
	[dueDate] [date] NULL,
	[frequencyOfPaymentPerYear] [int] NULL,
	[type] [varchar](max) NULL,
	[user_id] [int] NULL,
	[documentCategory_id] [int] NULL,
	[group_id] [int] NULL,
 CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDocumentCategory]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDocumentCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[documentCategory] [varchar](250) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEarningsCategory]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEarningsCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[EarningCategory] [varchar](50) NULL,
 CONSTRAINT [PK_EarningsCategory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[note] [varchar](250) NULL,
 CONSTRAINT [PK_tblGroup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroupUser]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroupUser](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[group_id] [int] NULL,
	[member_id] [int] NULL,
 CONSTRAINT [PK_tblGroupUser] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMember]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMember](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[identificator] [varchar](50) NULL,
	[note] [varchar](50) NULL,
 CONSTRAINT [PK_tblMember] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblNotification]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNotification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[note] [varchar](250) NULL,
	[document_id] [int] NULL,
	[user_id] [int] NULL,
	[dateOfPay] [date] NULL,
 CONSTRAINT [PK_tblNotification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRole]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRole](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[identificator] [varchar](50) NULL,
	[description] [varchar](50) NULL,
 CONSTRAINT [PK_tblRole] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 15. 4. 2019 14:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [varchar](50) NOT NULL,
	[lastName] [varchar](50) NOT NULL,
	[login] [varchar](50) NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NULL,
	[role_id] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCashFlow]  WITH CHECK ADD  CONSTRAINT [FK_tblCashFlow_tblCategory] FOREIGN KEY([eaCategory_id])
REFERENCES [dbo].[tblEarningsCategory] ([id])
GO
ALTER TABLE [dbo].[tblCashFlow] CHECK CONSTRAINT [FK_tblCashFlow_tblCategory]
GO
ALTER TABLE [dbo].[tblCashFlow]  WITH CHECK ADD  CONSTRAINT [FK_tblCashFlow_tblCategory1] FOREIGN KEY([exCategory_id])
REFERENCES [dbo].[tblCategory] ([id])
GO
ALTER TABLE [dbo].[tblCashFlow] CHECK CONSTRAINT [FK_tblCashFlow_tblCategory1]
GO
ALTER TABLE [dbo].[tblCashFlow]  WITH CHECK ADD  CONSTRAINT [FK_tblCashFlow_tblUser] FOREIGN KEY([user_id])
REFERENCES [dbo].[tblUser] ([id])
GO
ALTER TABLE [dbo].[tblCashFlow] CHECK CONSTRAINT [FK_tblCashFlow_tblUser]
GO
ALTER TABLE [dbo].[tblDocument]  WITH CHECK ADD  CONSTRAINT [FK_Document_tblUser] FOREIGN KEY([user_id])
REFERENCES [dbo].[tblUser] ([id])
GO
ALTER TABLE [dbo].[tblDocument] CHECK CONSTRAINT [FK_Document_tblUser]
GO
ALTER TABLE [dbo].[tblDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblDocument_tblDocumentCategory] FOREIGN KEY([documentCategory_id])
REFERENCES [dbo].[tblDocumentCategory] ([id])
GO
ALTER TABLE [dbo].[tblDocument] CHECK CONSTRAINT [FK_tblDocument_tblDocumentCategory]
GO
ALTER TABLE [dbo].[tblDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblDocument_tblGroup] FOREIGN KEY([group_id])
REFERENCES [dbo].[tblGroup] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblDocument] CHECK CONSTRAINT [FK_tblDocument_tblGroup]
GO
ALTER TABLE [dbo].[tblGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_tblGroupUser_tblGroup] FOREIGN KEY([group_id])
REFERENCES [dbo].[tblGroup] ([id])
GO
ALTER TABLE [dbo].[tblGroupUser] CHECK CONSTRAINT [FK_tblGroupUser_tblGroup]
GO
ALTER TABLE [dbo].[tblGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_tblGroupUser_tblMember] FOREIGN KEY([member_id])
REFERENCES [dbo].[tblMember] ([id])
GO
ALTER TABLE [dbo].[tblGroupUser] CHECK CONSTRAINT [FK_tblGroupUser_tblMember]
GO
ALTER TABLE [dbo].[tblGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_tblGroupUser_tblUser] FOREIGN KEY([user_id])
REFERENCES [dbo].[tblUser] ([id])
GO
ALTER TABLE [dbo].[tblGroupUser] CHECK CONSTRAINT [FK_tblGroupUser_tblUser]
GO
ALTER TABLE [dbo].[tblNotification]  WITH CHECK ADD  CONSTRAINT [FK_tblNotification_tblDocument] FOREIGN KEY([document_id])
REFERENCES [dbo].[tblDocument] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblNotification] CHECK CONSTRAINT [FK_tblNotification_tblDocument]
GO
ALTER TABLE [dbo].[tblNotification]  WITH CHECK ADD  CONSTRAINT [FK_tblNotification_tblUser] FOREIGN KEY([user_id])
REFERENCES [dbo].[tblUser] ([id])
GO
ALTER TABLE [dbo].[tblNotification] CHECK CONSTRAINT [FK_tblNotification_tblUser]
GO
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblRole] FOREIGN KEY([role_id])
REFERENCES [dbo].[tblRole] ([id])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblRole]
GO
USE [master]
GO
ALTER DATABASE [SpravaFinanci] SET  READ_WRITE 
GO
