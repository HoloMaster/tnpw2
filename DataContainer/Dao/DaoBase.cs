using System;
using System.Collections;
using System.Collections.Generic;
using DataContainer.Interface;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;

namespace DataContainer.Dao
{
    public class DaoBase<E>: IDaoBase<E> where E: class, IEntity
    {

        protected ISession session;

        protected DaoBase()
        {
            session = NHibernateHelper.Session;
        }


        public IList<E> GetAll()
        {
            Console.Write(session.QueryOver<E>().List<E>().ToString());
            return session.QueryOver<E>().List<E>();
           
        }

        public object Create(E entity)
        {
            object s;
            using (ITransaction tr = session.BeginTransaction())
            {
                s = session.Save(entity);
                tr.Commit();
            }

            return s;
            
        }

        public void Update(E entity)
        {
          
            using (ITransaction tr = session.BeginTransaction())
            {
                session.Update(entity);
                tr.Commit();
            }

           
        }

        public void Delete(E entity)
        {
             using (ITransaction tr = session.BeginTransaction())
             {
                 session.Delete(entity);
                           tr.Commit();
                       }
        }

        public E GetById(int? id)
        {
            return session.CreateCriteria<E>().Add(Restrictions.Eq("Id", id)).UniqueResult<E>();
        }
    }
}