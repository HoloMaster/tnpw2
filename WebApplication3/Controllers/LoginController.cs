using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using DataContainer.Dao;
using DataContainer.Model;
using WebApplication3.Class;

namespace WebApplication3.Controllers
{
  
    public class LoginController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ResetPssword()
        {
            return View();
        }


        public ActionResult ResetPassWord(string login,string newPassword)
        {
            try
            {
                UserDao userDao = new UserDao();
                User user = userDao.GetByLogin(login);
                SHA256 sha256 = new SHA256CryptoServiceProvider();
                Byte [] hashedPassword = sha256.ComputeHash(GetBytesForPassword(newPassword));
                newPassword = Encoding.ASCII.GetString(hashedPassword);
                user.Password = newPassword;
                userDao.Update(user);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                TempData["message-failed"] = "Login nebo heslo neni spravne";
            }
            
            
            return RedirectToAction("Index", "Login");
        }

    
        public ActionResult SignIn(string login, string password)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            Byte [] hashedPassword = sha256.ComputeHash(GetBytesForPassword(password));
            password = Encoding.ASCII.GetString(hashedPassword);
            if (Membership.ValidateUser(login,password))
            {
                if (Roles.IsUserInRole(login,"admin"))
                {
                    FormsAuthentication.SetAuthCookie(login,false);
                    return RedirectToAction("AdminIndex", "Home", new { area="Admin"});  
                }if (Roles.IsUserInRole(login,"normalUser"))
                {
                    FormsAuthentication.SetAuthCookie(login,false);
                    return RedirectToAction("Index", "Home"); 
                }
                
                
            }

            TempData["message-failed"] = "Login nebo heslo neni spravne";
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            return RedirectToAction("Index", "Login");
        }
        
        private byte[] GetBytesForPassword(string password)
        {
            return Encoding.ASCII.GetBytes(password);
        }

        
        


    }
}