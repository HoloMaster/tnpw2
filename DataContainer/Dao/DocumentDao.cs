using System;
using System.Collections.Generic;
using DataContainer.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class DocumentDao : DaoBase<Document>
    {
        public DocumentDao() : base()
        {}

        public IList<Document> GetDocumentPaged(int count, int pageNumber, out int totalDocuments, int userId)
        {
         
            
            totalDocuments = session.CreateCriteria<Document>()
                .SetProjection(Projections.RowCount()).UniqueResult<int>();

            return session.CreateCriteria<Document>()
                .Add(Restrictions.Eq("UserId.Id",userId))
                .AddOrder(Order.Asc("Name"))
                .SetFirstResult((pageNumber - 1) * count)
                .SetMaxResults(count)
                .List<Document>();
        }
        
      
        
        

        public IList<Document> SearchBooks(string phrase, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<Document>()
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Like("Name", $"%{phrase}%"))
                .SetFirstResult((pageNumber-1)* count)
                .SetMaxResults(count)
                .List<Document>();
        }

        public IList<Document> SearchBooks(string phrase, int userId)
        {
            return session.CreateCriteria<Document>()
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Like("Name", $"%{phrase}%"))
                .List<Document>();
        }
        
        public IList<Document> GetDocumentInCategoryId(int id, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<Document>()
                .CreateAlias("DocumentCategory", "docCat")
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Eq("docCat.Id", id))
                .SetFirstResult((pageNumber-1)* count)
                .SetMaxResults(count)
                .List<Document>();
        }
        
        public IList<Document> GetDocumentFilteredByDate(DateTime startDate, DateTime endDate, int count,
            int pageNumber, int userId)
        {
            return session.CreateCriteria<Document>()
                .AddOrder(Order.Asc("DateOfEstablishment"))
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Between("DateOfEstablishment", startDate, endDate))
                .SetFirstResult((pageNumber-1)* count)
                .SetMaxResults(count)
                .List<Document>();
        }

        public void DeleteNotificaitionFromDocuments(int id)
        {
            
            IList<Notification> notifications = session.CreateCriteria<Notification>()
                .CreateAlias("Document", "notDoc")
                .Add(Restrictions.Eq("notDoc.Id", id))
                .List<Notification>();
            
            using (ITransaction tr = session.BeginTransaction())
            {
                foreach(Notification notification in notifications)
                {
                    session.Delete(notification);
                }
                tr.Commit();
                    
            }
            
            
        }

       
    }
}