using DataContainer.Dao;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class Role : IEntity
    {
        public virtual int Id { get; set; }
        
        public virtual string Identificator { get; set; }
        
        public virtual string Description { get; set; }
        
        
        public static Role SetDefaultRol(){
            RoleDao roleDao = new RoleDao();
            return roleDao.GetDefaultRole();
        }
        
        public static Role SetAdminRol(){
            RoleDao roleDao = new RoleDao();
            return roleDao.GetGroupLeaderRole();
        }
        
   
        
    }
    
    
}