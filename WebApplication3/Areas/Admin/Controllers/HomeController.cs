using System.Web.Mvc;

namespace WebApplication3.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public ActionResult AdminIndex()
        {
            return
            View();
        }
    }
}