using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserController : Controller
    {
        // GET
        public ActionResult AdminIndex(int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;
            
            IList<User> users = new UserDao().GetUserPaged(itemsOnPage,pg,out totalBooks);
            ViewBag.Users = users;
            
            
            
            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;
            
            return
            View();
        }

        public ActionResult Index()
        {
            UserDao userDao = new UserDao();
            User user = userDao.GetByLogin(User.Identity.Name);
           
           
                
            return View(user);
        }
        public ActionResult AdminDelete(int id)
        {
            try
            {
                UserDao userDao = new UserDao();
                userDao.RemoveCashFlowAfterDeletingUser(id);
                userDao.DeleteDocumentsAfterDeletingUser(id);
                userDao.RemoveGroupMembershipAfterDeletingUser(id);
            
                User user = userDao.GetById(id);
                userDao.Delete(user);
                
            }
            catch (Exception e)
            {
                
                TempData["message-failed"] = "Něco se nepovedlo :(";
                return RedirectToAction("AdminIndex","User");
            }
            
           
            
            return RedirectToAction("AdminIndex","User");
        }
        

        public ActionResult AddAdmin(User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                  
                   UserDao userDaouser = new UserDao();
                   user.Role = Role.SetAdminRol();
                   SHA256 sha256 = new SHA256CryptoServiceProvider();
                   Byte [] hashedPassword = sha256.ComputeHash( Encoding.ASCII.GetBytes(user.Password));
                   user.Password = Encoding.ASCII.GetString(hashedPassword);
                   
                   
                   
                   userDaouser.Create(user);


                   
                        TempData["message-success"] = "Výdaj "+ user.FirstName + " "+user.LastName+" úspěšně přidán"; 
                    
                   
                
                 
                
                }
                else
                {
                    return View("AdminAdd",user);
                }

                return RedirectToAction("AdminIndex");


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return RedirectToAction("AdminIndex");
            }
            
            
        }

        public ActionResult AdminAdd()
        {
            return View();
        }

       
        public ActionResult Edit(int id)
        {
            UserDao ud = new UserDao();
            User user = ud.GetById(id);
            
            return View(user);
            
        }
        
        public ActionResult Detail(int id)
        {
            UserDao ud = new UserDao();
            User user = ud.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(user);
            }
            
            
            return View(user);
            
        }

        [HttpPost]
        public ActionResult Update(User u)
        {
            try
            {
                UserDao ud = new UserDao(); 
                ud.Update(u);
        
                 
                TempData["message-success"] = "Uživatel "+ u.FirstName +" "+ u.LastName+" úspěšně aktualizován";

            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se nepovedlo :(";
                return RedirectToAction("AdminIndex","User");
            }
                   
            return RedirectToAction("Index", "User", new {area= "Admin"});
        
        }
    }





    }
