using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using ZipFile = Ionic.Zip.ZipFile;

namespace WebApplication3.utils
{
    public class Encryption
    {

        public ZipFile SecureImage(Bitmap b,string password, string wayToZip)
        {

            using (var zip = new ZipFile())
            {
                zip.Password = password;
                zip.AddFile(wayToZip,b.ToString());
                return zip;
            }
           
        }
        
        
        public Bitmap DecryptImage(Bitmap b)
        {

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                b.Save(memoryStream,ImageFormat.Bmp);
                bytes = memoryStream.ToArray();    
            }
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            Byte[] hashedImage = sha256.ComputeHash(bytes);
            
            Bitmap bmp;
            using (var ms = new MemoryStream(hashedImage))
            {
                bmp = new Bitmap(ms);
            }

            return bmp;
        }
        
        
    }
}