using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Web.UI.WebControls;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class CashFlow : IEntity
    {
        public virtual int Id { get; set; }
        
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d. M. yyyy}")]
        [DataType(DataType.DateTime, ErrorMessage = "Vyberte prosim datum")]
        public virtual DateTime DateOfPayment { get; set; }
        [Required(ErrorMessage = "Částka je vyžadována")]
        [RegularExpression( @"^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$",ErrorMessage = "Chybný fomrát. např 1200.20")]
        public virtual string Amount { get; set; }
        
        
        public virtual String Note { get; set; }

        public virtual EarningCategory CategoryOfItem { get; set; }
        
        public virtual ExpenditureCategory CategoryOfItem1 { get; set; }

        public virtual User UserId { get; set; }

        public virtual bool Earning { get; set; }



        public virtual string GetAmountFromStringAndBack(string stringin)
        {

            decimal dec = decimal.Parse(stringin);
            return stringin = dec.ToString();
        }
        
    }
}