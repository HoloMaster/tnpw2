using System.ComponentModel;
using DataContainer.Dao;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class Member : IEntity
    {
        public virtual int Id { get; set; }
        
        public virtual string Identificator { get; set; }
        
        public virtual string Note { get; set; }
        
        public static Member SetDefaultRol(){
            MemberDao memberDao = new MemberDao();
            return memberDao.GetDefaultRole();
        }
        
        public static Member SetGroupLeaderRol(){
            MemberDao memberDao = new MemberDao();
            return memberDao.GetGroupLeaderRole();
        }

        
        
        
    }
}