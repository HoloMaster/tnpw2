﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class Document : IEntity
    {
        
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Nazev dokumentu je vyzadovan")]
        
        public virtual string Name { get; set; }
       
       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d. M. yyyy}")]
       [DataType(DataType.Date, ErrorMessage = "Vyberte prosim datum")]
        public virtual DateTime DateOfEstablishment { get; set; }
        
  
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d. M. yyyy}")]
        [DataType(DataType.Date, ErrorMessage = "Vyberte prosim datum")]
        public virtual DateTime DueDate { get; set; }
        
        [Range(0,12, ErrorMessage = "Zadejte prosim rozmezi 0-12")]
        public virtual int FrequencyOfPaymentPerYear { get; set; }

        public virtual String Type { get; set; }

        public virtual DocumentCategory DocumentCategory { get; set; }

        public virtual User UserId { get; set; }
        public virtual Group Group { get; set; }
    }

}
