using System;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {

        public ActionResult Index()
        {
            EarningCategoryDao earningCategoryDao = new EarningCategoryDao();
            ExpenditureCategoryDao expenditureCategoryDao = new ExpenditureCategoryDao();
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();

            ViewBag.EaCategory = earningCategoryDao.GetAll();
            ViewBag.ExCategory = expenditureCategoryDao.GetAll();
            ViewBag.DocCategory = documentCategoryDao.GetAll();
            
            
            return View();
        }

        public ActionResult CreateDocCategory()
        {
            
            return PartialView();
        }
        
        [HttpPost]
        public ActionResult AddDocCategory(DocumentCategory documentCategory)
        {

            if (ModelState.IsValid)
            {
                
                
               DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
               documentCategoryDao.Create(documentCategory);
                
                
                TempData["message-success"] = "Kategorie "+ documentCategory.Category+" úspěšně vytvořena";
                
                 
                
            }
            else
            {
                return View("CreateDocCategory", documentCategory);
            }

            return RedirectToAction("Index");
        }
        public ActionResult CreateEaCategory()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddEaCategory(EarningCategory category)
        {

            if (ModelState.IsValid)
            {
                
                
                EarningCategoryDao earningCategoryDao = new EarningCategoryDao();
                earningCategoryDao.Create(category);
                
                TempData["message-success"] = "Kategorie "+ category.Note+" úspěšně vytvořena";
                
                 
                
            }
            else
            {
                return View("CreateEaCategory", category);
            }

            return RedirectToAction("Index");
        }
        public ActionResult CreateExCategory()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddExCategory(ExpenditureCategory category)
        {

            if (ModelState.IsValid)
            {
                
                
                ExpenditureCategoryDao expenditureCategoryDao = new ExpenditureCategoryDao();
                expenditureCategoryDao.Create(category);
                
                
                TempData["message-success"] = "Kategorie "+ category.Note+" úspěšně vytvořena";
                
                 
                
            }
            else
            {
                return View("CreateExCategory", category);
            }

            return RedirectToAction("Index");
        }

        public ActionResult DeleteExCategory(int id)
        {
            try
            {
               ExpenditureCategory ex = new ExpenditureCategoryDao().GetById(id);
               new ExpenditureCategoryDao().Delete(ex);
               
                
                
                TempData["message-success"] = "Kategorie "+ ex.Note+" úspěšně smazna";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
               
           
            return RedirectToAction("Index");
        }
        public ActionResult DeleteEaCategory(int id)
        {
            try
            {
            EarningCategory ea = new EarningCategoryDao().GetById(id);
            new EarningCategoryDao().Delete(ea);
                
                TempData["message-success"] = "Kategorie "+ ea.Note+" úspěšně smazna";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
               
                 
            
            return RedirectToAction("Index");
        }
        public ActionResult DeleteDocCategory(int id)
        {
            try
            {
            DocumentCategory dc = new DocumentCategoryDao().GetById(id);
            new DocumentCategoryDao().Delete(dc);
                TempData["message-success"] = "Kategorie "+ dc.Category+" úspěšně smazna";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
                
           
            return RedirectToAction("Index");
        }
        
    }
}