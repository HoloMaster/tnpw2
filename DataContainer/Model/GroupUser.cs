using DataContainer.Interface;

namespace DataContainer.Model
{
    public class GroupUser : IEntity
    {
        public virtual User User { get; set; }

        public virtual Group Group { get; set; }
        
        public virtual Member Member { get; set; }
        
        public virtual int Id { get; set; }
    }
}