using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    public class NotificationController : Controller
    {
        [Authorize(Roles = "normalUser")]
        // GET
        public ActionResult Index(int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
           
            IList<Notification> notifications =
                new NotificationDao().GetNotificationFilteredByDate(startDate, endDate, itemsOnPage, pg,user.Id);

           
           
            
            int totalBooks = notifications.Count;

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            

            
            return
            View(notifications);
        }
        
        public ActionResult FilterByDate(DateTime? startDate, DateTime? endDate, int? page)
        {
            try
            {
                int itemsOnPage = 10;
                int pg = page.HasValue ? page.Value : 1;
                User user = new UserDao().GetByLogin(User.Identity.Name);
                if (startDate == DateTime.MinValue)
                {
                    startDate = DateTime.Today; 
                }

                if (endDate == DateTime.MinValue)
                {
                    endDate = DateTime.Today.AddMonths(1).AddDays(-1);
                }
           
            
               
           
                IList<Notification> notifications =
                    new NotificationDao().GetNotificationFilteredByDate(startDate,endDate,itemsOnPage,pg,user.Id);
               
                int totalBooks = notifications.Count;

                ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                ViewBag.CurrentPage = pg;
                TempData["aktualni_datum"] = startDate.GetValueOrDefault().ToShortDateString()+" - "+ endDate.GetValueOrDefault().ToShortDateString();

           

                if (Request.IsAjaxRequest())
                {
                    return PartialView("Index", notifications);
                }

                return View("Index", notifications);
            }
            catch (Exception e)
            {TempData["message-failed"] = "Něco se pokazilo :( ";
             return RedirectToAction("Index", "Group");
            }
           
        }
        public ActionResult Delete(int id)
        {
            try
            {
                NotificationDao notificationDao = new NotificationDao();
                Notification notification = notificationDao.GetById(id);
               

                notificationDao.Delete(notification);

                TempData["message-success"] = "Upozornění "+notification.Note +" bylo smazáno";
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Group");
            }

            return RedirectToAction("Index");
        }

    }
}