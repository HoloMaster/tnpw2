using System.Web.Security;
using DataContainer.Model;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class RoleDao : DaoBase<Role>
    {
        public Role GetDefaultRole()
        {
            return session.CreateCriteria<Role>().Add(Restrictions.Eq("Identificator",
                "normalUser")).UniqueResult<Role>();
        }
        
        public Role GetGroupLeaderRole()
        {
            return session.CreateCriteria<Role>().Add(Restrictions.Eq("Identificator",
                "admin")).UniqueResult<Role>();
        }
        
    }
}