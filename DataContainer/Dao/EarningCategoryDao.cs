using System.Collections.Generic;
using DataContainer.Model;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class EarningCategoryDao : DaoBase<EarningCategory>
    {

        public EarningCategoryDao() : base()
        {}
            
            
            public IList<EarningCategory> GetEarningCategory()
            {
                return session.CreateCriteria<EarningCategory>()
                   .List<EarningCategory>();
            }
        

    }
}