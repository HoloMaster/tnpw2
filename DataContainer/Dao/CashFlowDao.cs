using System;
using System.Collections.Generic;
using DataContainer.Model;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class CashFlowDao : DaoBase<CashFlow>
    {
        public CashFlowDao() : base()
        {
        }
        
        public IList<CashFlow> SearchCashFlows(string phrase, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<CashFlow>()
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Like("Note", $"%{phrase}%"))
                .SetFirstResult((pageNumber-1)* count)
                .SetMaxResults(count)
                .List<CashFlow>();
        }
        
        public IList<CashFlow> GetCashFlowByEaCategoryId(int id, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<CashFlow>()
                .CreateAlias("CategoryOfItem", "docCat")
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Eq("docCat.Id", id))
                .SetFirstResult((pageNumber - 1) * count)
                .SetMaxResults(count)
                .List<CashFlow>();
        }
        
        public IList<CashFlow> GetCashFlowByExCategoryId(int id, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<CashFlow>()
                .CreateAlias("CategoryOfItem1", "docCat")
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Eq("docCat.Id", id))
                .SetFirstResult((pageNumber - 1) * count)
                .SetMaxResults(count)
                .List<CashFlow>();
        }


        public IList<CashFlow> GetCasFlowFilteredByDate(DateTime startDate, DateTime? endDate, int count,
            int pageNumber, int userId)
        {
           return session.CreateCriteria<CashFlow>()
                           .Add(Restrictions.Eq("UserId.Id",userId))
                           .AddOrder(Order.Desc("DateOfPayment"))
                           .Add(Restrictions.Between("DateOfPayment", startDate, endDate))
                           .SetFirstResult((pageNumber - 1) * count)
                           .SetMaxResults(count)
                           .List<CashFlow>();
        }
        
        public IList<CashFlow> SelectOnlyX(bool phrase, int count, int pageNumber, int userId)
        {
            return session.CreateCriteria<CashFlow>()
                .Add(Restrictions.Eq("UserId.Id",userId))
                .Add(Restrictions.Like("Earning", phrase))
                .SetFirstResult((pageNumber - 1) * count)
                .SetMaxResults(count)
                .List<CashFlow>();
        }


        public IList<CashFlow> GetCashFlowPaged(int count, int pageNumber, out int totalCashFlows, int userId)
        {
            totalCashFlows = session.CreateCriteria<CashFlow>()
                            .SetProjection(Projections.RowCount()).UniqueResult<int>();
            
                        return session.CreateCriteria<CashFlow>()
                            .Add(Restrictions.Eq("UserId.Id",userId))
                            .AddOrder(Order.Desc("DateOfPayment"))
                            .SetFirstResult((pageNumber - 1) * count)
                            .SetMaxResults(count)
                            .List<CashFlow>();
                    }

        public IList<CashFlow> GetAllForThisMonthUser(User getByLogin)
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            return session.CreateCriteria<CashFlow>()
                .Add(Restrictions.Eq("UserId.Id", getByLogin.Id))
                .Add(Restrictions.Between("DateOfPayment",startDate,endDate))
                .List<CashFlow>();
        }
        public IList<CashFlow> GetAllForUser(User getByLogin)
        {
          
            return session.CreateCriteria<CashFlow>()
                .Add(Restrictions.Eq("UserId.Id", getByLogin.Id))
                .List<CashFlow>();
        }
    }
    }
