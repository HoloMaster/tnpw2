using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "normalUser")]
    public class GroupController : Controller
    {
        public ActionResult Index()
        {
            UserDao userDao = new UserDao();
            GroupUserDao groupUserDao = new GroupUserDao();
            IList<GroupUser> groupUser =  groupUserDao.GetGroupWhereUser(userDao.GetByLogin(User.Identity.Name));
            return View(groupUser);
        }
        
       
        public ActionResult Detail(int id)
        {
           
            Group group= new GroupDao().GetById(id);
            IList<GroupUser> gus = new GroupUserDao().GetGroupWhereGroup(id);
            IList<User> users = new List<User>();
            
            foreach (GroupUser userG in gus)
            {
                if (new GroupUserDao().IsUserInThisGroup(new UserDao().GetByLogin(userG.User.Login), id))
                {
                    if (userG.Member.Identificator != "adminGroup")
                    {
                        users.Add(userG.User); 
                    }
                    
                }

               
                
            }

            ViewBag.Users = users;

            if (Request.IsAjaxRequest())
            {
                return PartialView(group);
            }
           

                return View(group);
        }
        public ActionResult DetailOfDocuments(int id)
        {
           
            Group group= new GroupDao().GetById(id);
            IList<Document> d = new GroupUserDao().GetGroupWhereDocument(id);



            ViewBag.Documents = d;

            if (Request.IsAjaxRequest())
            {
                return PartialView(group);
            }
           

            return View(group);
        }

       
        public ActionResult Create()
        {
            IList<Group> groups = new GroupDao().GetAll();

            ViewBag.Groups = groups;
            
           
            return View();
        }

        [HttpPost]
        public ActionResult Add(Group group)
        {
            try
            {
                if (ModelState.IsValid)
                {
                
                
                    GroupUserDao groupUserDao = new GroupUserDao();
                
                    UserDao userDao = new UserDao();
               
                    GroupUser groupUser = new GroupUser();
                    groupUser.Group = group;
                    groupUser.User = userDao.GetByLogin(User.Identity.Name);
                    groupUser.Member = Member.SetGroupLeaderRol();
                                
                    GroupDao groupDao = new GroupDao();
                    groupDao.Create(group);

                    groupUserDao.Create(groupUser);
                
                
                    TempData["message-success"] = "Skupina "+ groupUser.Group.Name+" úspěšně vytvořena";
                
                 
                
                }
                else
                {
                    return View("Create", group);
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Group");
            }

           
        }
        
      
        public ActionResult AddUserToGroup(GroupUser user)
        {
            try
            {
                GroupUserDao groupUserDao = new GroupUserDao();
                IList<GroupUser> groupUsers = groupUserDao.GetGroupWhereUserGroupAdmin(new UserDao().GetByLogin(User.Identity.Name));
                IList<Group> groups = new List<Group>();
                foreach (GroupUser g in groupUsers)
                {
                    groups.Add(g.Group);
                }
            
                ViewBag.Groups = groups;
               
                return PartialView();
            }
            catch (Exception e)
            { TempData["message-failed"] = "Něco se pokazilo :( ";
                             return RedirectToAction("Index", "Group");
            }
        
        }
        
   
        public ActionResult AddUser(int groupId,string login)
        {
            GroupUser gu = new GroupUser();
            try
            {
                bool inThisGroup = new GroupUserDao().IsUserInThisGroup(new UserDao().GetByLogin(login), groupId);
            
                
                if (ModelState.IsValid && !inThisGroup)
                {
                    GroupUserDao groupUserDao = new GroupUserDao();
                    gu.Member = Member.SetDefaultRol();
                    gu.Group = new GroupDao().GetById(groupId);
                    gu.User = new UserDao().GetByLogin(login);
                    groupUserDao.Create(gu); 
                    
                }
                else
                {
                    return PartialView("AddUserToGroup");
                }
                
                TempData["message-success"] = "Uživatel "+ gu.User.FirstName +" "+ gu.User.LastName+" úspěšně vytvořena";
                
                

            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Group");
            }
           
            
          

            return RedirectToAction("Index");
        }

        public ActionResult DeleteUserFromGroup(int userId,int groupId)
        {
            try
            {
                GroupUserDao groupUserDao = new GroupUserDao();
                GroupUser groupUser = groupUserDao.GetUserGroupForDelete(userId,groupId);

                groupUserDao.Delete(groupUser);

               
                TempData["message-success"] = "Uživatel "+ groupUser.User.FirstName +" "+ groupUser.User.LastName+" úspěšně odstraněn";
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Group");
            }

            return RedirectToAction("Index");
            
            
        }

        public ActionResult DeleteGroup(int groupId)
        {
            GroupUserDao groupUserDao = new GroupUserDao();
            groupUserDao.Delete(groupUserDao.GetGroupWhereGroup(groupId));
            GroupDao groupDao = new GroupDao();
            groupDao.Delete(groupDao.GetById(groupId));
            
            return RedirectToAction("Index");
        }

    }
}