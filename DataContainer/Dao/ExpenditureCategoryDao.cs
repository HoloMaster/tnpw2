using DataContainer.Model;

namespace DataContainer.Dao
{
    public class ExpenditureCategoryDao : DaoBase<ExpenditureCategory>
    {
        public ExpenditureCategoryDao() : base()
        {
        }
    }
}