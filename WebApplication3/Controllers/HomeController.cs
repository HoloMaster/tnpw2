﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "normalUser")]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<CashFlow> cashFlows = new CashFlowDao().GetAllForThisMonthUser(new UserDao().GetByLogin(User.Identity.Name));
            IList<CashFlow> cashFlows2 = new CashFlowDao().GetAllForUser(new UserDao().GetByLogin(User.Identity.Name));

            double total = 0;
            double totalForAll = 0;

            foreach (CashFlow c in cashFlows)
            {
                if (c.Earning)
                {
                    double pomoc = double.Parse(c.Amount,CultureInfo.InvariantCulture);
                
                    total = total + pomoc ;
                }
                else
                {
                    double pomoc = double.Parse(c.Amount, CultureInfo.InvariantCulture);
                   
                    total = total - pomoc;
                }
            }
            foreach (CashFlow c in cashFlows2)
            {
                if (c.Earning)
                {
                    double pomoc = double.Parse(c.Amount,CultureInfo.InvariantCulture);
                    
                    totalForAll = totalForAll + pomoc ;
                }
                else
                {
                    double pomoc = double.Parse(c.Amount, CultureInfo.InvariantCulture);
                 
                    totalForAll = totalForAll - pomoc;
                }
            }
            total = Math.Round(total, 2);
            totalForAll = Math.Round(totalForAll, 2);

            Tuple<User,double,double> tupleData = new Tuple<User, double,double>(user,total,totalForAll);

            
            return View(tupleData);
        }

        public ActionResult ChartForCashFlow()
        {
            IList<CashFlow> cashFlows = new CashFlowDao().GetAllForThisMonthUser(new UserDao().GetByLogin(User.Identity.Name));
           
            double total = 0;
            double totalForAll = 0;

            
            foreach (CashFlow c in cashFlows)
            {
                if (c.Earning)
                {
                    double pomoc = double.Parse(c.Amount,CultureInfo.InvariantCulture);                    
                    total = total + pomoc ;
                    
                }
                else
                {
                    double pomoc = double.Parse(c.Amount, CultureInfo.InvariantCulture);
                 
                    totalForAll = totalForAll + pomoc;
                }
            }
            total = Math.Round(total, 2);
            totalForAll = Math.Round(totalForAll, 2);

            string[] graph = {total.ToString(), totalForAll.ToString()};
            
            
            return View(graph);
        }
        public ActionResult ChartForCashFlow2()
        {
            IList<CashFlow> cashFlows = new CashFlowDao().GetAllForUser(new UserDao().GetByLogin(User.Identity.Name));
           
            double total = 0;
            double totalForAll = 0;

            
            foreach (CashFlow c in cashFlows)
            {
                if (c.Earning)
                {
                    double pomoc = double.Parse(c.Amount,CultureInfo.InvariantCulture);                    
                    total = total + pomoc ;
                    
                }
                else
                {
                    double pomoc = double.Parse(c.Amount, CultureInfo.InvariantCulture);
                 
                    totalForAll = totalForAll + pomoc;
                }
            }
            total = Math.Round(total, 2);
            totalForAll = Math.Round(totalForAll, 2);

            string[] graph = {total.ToString(), totalForAll.ToString()};
            
            return View(graph);
        }


      
    }
}