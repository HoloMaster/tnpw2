using DataContainer.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class MemberDao : DaoBase<Member>
    {
        public MemberDao() : base(){}
        
        public Member GetDefaultRole()
        {
            return session.CreateCriteria<Member>().Add(Restrictions.Eq("Identificator",
                "member")).UniqueResult<Member>();
        }
        
        public Member GetGroupLeaderRole()
        {
            return session.CreateCriteria<Member>().Add(Restrictions.Eq("Identificator",
                "adminGroup")).UniqueResult<Member>();
        }

        
        
        
    }
}