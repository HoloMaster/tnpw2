using DataContainer.Interface;

namespace DataContainer.Model
{
    public class EarningCategory : IEntity
    {
        public virtual int Id { get; set; }

        public virtual string Note { get; set; }
    }
}