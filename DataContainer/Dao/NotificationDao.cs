using System;
using System.Collections.Generic;
using DataContainer.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class NotificationDao : DaoBase<Notification>
    {
        public NotificationDao() : base(){}
        
        
        public IList<Notification> GetNotificationFilteredByDate(DateTime? startDate, DateTime? endDate, int count,
            int pageNumber, int userId)
        {
            if (startDate == DateTime.MinValue)
            {
                startDate = DateTime.Now.AddDays(-1); 
            }

            if (endDate == DateTime.MinValue)
            {
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                    DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }
            return session.CreateCriteria<Notification>()
                .AddOrder(Order.Asc("DateOfPay"))
                .Add(Restrictions.Eq("User.Id",userId))
                .Add(Restrictions.Between("DateOfPay", startDate, endDate))
                .SetFirstResult((pageNumber-1)* count)
                .SetMaxResults(count)
                .List<Notification>();
        }
        
        public IList<Notification> GetNotificationByUser(int id)
        {
            
            IList<Notification> notifications = session.CreateCriteria<Notification>()
                .Add(Restrictions.Eq("Document.Id", id))                
                .List<Notification>();
            
            session.Flush();
            session.Clear();
           
            return notifications;
        }
        
        public int GetNotificationByUserForCount(int id,DateTime? startDate, DateTime? endDate)
        {

            return session.CreateCriteria<Notification>()
                .Add(Restrictions.Eq("User.Id", id))
                .Add(Restrictions.Between("DateOfPay", startDate, endDate))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();



        }
        
     
        
        public void Create(IList<Notification> notifications)
        {
           using (ITransaction tx = session.BeginTransaction())
           {
               
               for (int i = 0; i < notifications.Count; i++)
               {
                   Notification notification = notifications[i];
                   session.Save(notification);
                   
                   if (i % notifications.Count-1 == 0)
                   {session.Flush();
                    session.Clear();
                   }
               }
           
               tx.Commit();
           }
            
        }
        
        public void Delete(int id)
        {
            using (ITransaction tx = session.BeginTransaction())
            {
                IList<Notification> notifications = GetNotificationByUser(id);
                for (int i = 0; i < notifications.Count; i++)
                {
                    Notification notification = notifications[i];
                    session.Delete(notification);
                   
                    if (i % notifications.Count-1 == 0)
                    {session.Flush();
                     session.Clear();
                    }
                }
           
                tx.Commit();
            }
            
        }
        

        
        
        
    }
}