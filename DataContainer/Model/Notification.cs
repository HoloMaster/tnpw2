using System;
using System.ComponentModel.DataAnnotations;
using DataContainer.Interface;

namespace DataContainer.Model
{
    public class Notification : IEntity
    {
        public virtual int Id { get; set; }
        
        public virtual User User { get; set; }
        
        public virtual Document Document { get; set; }
        
        public virtual string Note { get; set; }
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
     
        public virtual DateTime DateOfPay { get; set; }
    }
}