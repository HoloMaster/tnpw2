using System.Web.Security;
using DataContainer.Dao;
using DataContainer.Model;

namespace WebApplication3.Class
{
    public class SpravaFinanciRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            UserDao userDao = new UserDao();
            User user = userDao.GetByLogin(username);

            if (user == null)
            {
                return false;
            }

            return user.Role.Identificator == roleName;
        }

        public override string[] GetRolesForUser(string username)
        {
            UserDao userDao = new UserDao();
            User user = userDao.GetByLogin(username);
            if (user == null)
            {
                return new string[]{};
            }
            return new string[]{user.Role.Identificator};

        }

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}