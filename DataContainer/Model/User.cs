using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using DataContainer.Interface;


namespace DataContainer.Model
{
    public class User : MembershipUser, IEntity
    {
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Jmeno je vyzadovano")]
        
        public virtual String FirstName { get; set; }

      
        [Required(ErrorMessage = "Prijmeni je vyzadovano je vyzadovano")]
        public virtual String LastName { get; set; }

        public virtual String Login { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage =
            "E-mail neni validni")]
        [Required(ErrorMessage = "EmailOfUser je vyzadovan")]
        public virtual String EmailOfUser { get; set; }
        [Required(ErrorMessage = "Heslo je vyžadováno")]
        public virtual String Password { get; set; }

        public virtual Role Role { get; set; }

        public virtual String getLoginFromEmail(String email)
        {
            if (!String.IsNullOrWhiteSpace(email))
            {
                int chaLocation = email.IndexOf("@", StringComparison.Ordinal);
                if (chaLocation > 0)
                {
                    Console.Write(email.Substring(0,chaLocation));
                }

                {
                    
                }
            }

            return String.Empty;
        }

        public User()
        {
        }

        public User(string firstName, string lastName, string login, string emailOfUser, string password, Role role)
        {
            FirstName = firstName;
            LastName = lastName;
            Login = login;
            EmailOfUser = emailOfUser;
            Password = password;
            Role = role;
        }
      
    }
}