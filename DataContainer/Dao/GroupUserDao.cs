using System.Collections.Generic;
using DataContainer.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class GroupUserDao : DaoBase<GroupUser>
    {
        public GroupUserDao() : base(){}


        public IList<GroupUser> GetGroupWhereUser(User user)
        {
            return 
            session.CreateCriteria<GroupUser>()
                .CreateAlias("User","u")
                .Add(Restrictions.Eq("u.Id", user.Id))
                .List<GroupUser>();
        }
        
        
        
        public IList<GroupUser> GetGroupWhereGroup(int id)
        {
            return 
                session.CreateCriteria<GroupUser>()
                    .Add(Restrictions.Eq("Group.Id", id))
                    .List<GroupUser>();
        }
        public IList<Document> GetGroupWhereDocument(int id)
        {
            return 
                session.CreateCriteria<Document>()
                    .Add(Restrictions.Eq("Group.Id", id))
                    .List<Document>();
        }

        public int GetNumberOfUsersInGroup(int id)
        {
            return session.CreateCriteria<GroupUser>()
                .SetProjection(Projections.RowCount())
                .Add(Restrictions.Eq("Group", id))
                .UniqueResult<int>();
        }
        
        
        public IList<GroupUser> GetGroupWhereUserGroupAdmin(User user)
        {
            return 
                session.CreateCriteria<GroupUser>()
                    .CreateAlias("User","u")
                    .CreateAlias("Member","m")
                    .Add(Restrictions.Eq("u.Id", user.Id))
                    .Add(Restrictions.Eq("m.Id",1))
                    .List<GroupUser>();
        }
        
        public bool IsUserInThisGroup(User user,int groupId)
        {
            
            if (session.CreateCriteria<GroupUser>()
                    .CreateAlias("User", "u")
                    .CreateAlias("Group", "g")
                    .Add(Restrictions.Eq("u.Id", user.Id))
                    .Add(Restrictions.Eq("g.Id", groupId))
                    .UniqueResult<GroupUser>() != null){
                
                return true;
            }


            return false;

        }
        
        public bool IsUserInGroup(User user)
        {
            
            if (session.CreateCriteria<GroupUser>()
                    .CreateAlias("User", "u")
                    .Add(Restrictions.Eq("u.Id", user.Id))
                    .UniqueResult<GroupUser>() != null){
                
                return true;
            }


            return false;

        }
        

        public GroupUser GetUserGroupForDelete(int userId,int groupId)
        {
            return session.CreateCriteria<GroupUser>()
                .Add(Restrictions.Eq("User.Id", userId))
                .Add(Restrictions.Eq("Group.Id", groupId))
                .UniqueResult<GroupUser>();
        }
        
        
        public void Delete(IList<GroupUser> groupUsers)
        {
            using (ITransaction tx = session.BeginTransaction())
            {
                for (int i = 0; i < groupUsers.Count; i++)
                {
                    GroupUser groupUser = groupUsers[i];
                    session.Delete(groupUser);
                   
                    if (i % groupUsers.Count-1 == 0)
                    {
                        session.Flush();
                        session.Clear();
                    }
                }
           
                tx.Commit();
            }
            
        }
        
        
       
    }
}