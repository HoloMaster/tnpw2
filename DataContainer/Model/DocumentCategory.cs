using DataContainer.Interface;

namespace DataContainer.Model
{
    public class DocumentCategory : IEntity
    {        
       public virtual int Id { get; set; }

       public virtual string Category { get; set; }
    }
}