﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using DataContainer.Dao;
using DataContainer.Model;
using WebApplication3.Helpers;
using WebApplication3.utils;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "normalUser")]
    public class DocumentController : Controller
    {
        
        // GET: Document
        public ActionResult Index(int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            DocumentDao documentDao = new DocumentDao();
            IList<Document> documents = documentDao.GetDocumentPaged(itemsOnPage, pg, out totalBooks,user.Id);

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView(documents);
            }

            return View(documents);
        }

     
        public ActionResult Detail(int id)
        {
            DocumentDao documentDao = new DocumentDao();
            Document d = documentDao.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(d);
            }

            return View(d);
        }

    
        public ActionResult Category(int id,int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<Document> documents = new DocumentDao().GetDocumentInCategoryId(id,itemsOnPage,pg,user.Id);
            
            int totalBooks = documents.Count;
            
            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
            ViewBag.CurrentPage = pg;

            ViewBag.Categories = new DocumentCategoryDao().GetAll();
            
            
            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", documents);
            }

            return View("Index", documents);
        }

        public ActionResult FilterByDate(DateTime startDate, DateTime endDate, int? page)
        {
            try
            {
                int itemsOnPage = 10;
                int pg = page.HasValue ? page.Value : 1;
                User user = new UserDao().GetByLogin(User.Identity.Name);
                IList<Document> documents =
                    new DocumentDao().GetDocumentFilteredByDate(startDate, endDate, itemsOnPage, pg, user.Id);

                int totalBooks = documents.Count;

                ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                ViewBag.CurrentPage = pg;

                ViewBag.Categories = new DocumentCategoryDao().GetAll();

                if (Request.IsAjaxRequest())
                {
                    return PartialView("Index", documents);
                }

                return View("Index", documents);
            }
            catch (Exception e)
            {
                {
                    TempData["message-failed"] = "Něco se pokazilo :( ";
                    return RedirectToAction("Index", "Document");
                }

            }
        }


        public ActionResult Search(string phrase, int? page)
        {
            try
            {
                int itemsOnPage = 10;
                int pg = page.HasValue ? page.Value : 1;
                User user = new UserDao().GetByLogin(User.Identity.Name);
                DocumentDao documentDao = new DocumentDao();
                IList<Document> documents = documentDao.SearchBooks(phrase, itemsOnPage, pg,user.Id);
            
                int totalBooks = documents.Count;

                ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / itemsOnPage);
                ViewBag.CurrentPage = pg;

                ViewBag.Categories = new DocumentCategoryDao().GetAll();

                if (Request.IsAjaxRequest())
                {
                    return PartialView("Index", documents);
                }

                return View("Index", documents);

            }
            catch (Exception e)
            {TempData["message-failed"] = "Něco se pokazilo :( ";
                             return RedirectToAction("Index", "Document");
            }
           
        }

   
        public JsonResult SearchWithHelp(string query)
        {
            DocumentDao documentDao = new DocumentDao();
            User user = new UserDao().GetByLogin(User.Identity.Name);
            IList<Document> documents = documentDao.SearchBooks(query,user.Id);
            List<string> names = (from Document d in documents select d.Name).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            return PartialView();

        }
  
        public ActionResult CreateOnlyForUser()
        {
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
            IList<DocumentCategory> documentCategories = documentCategoryDao.GetAll();
            ViewBag.DocumentCategories = documentCategories;
            return View();
        }

        public ActionResult CreatForGroupAndUser()
        {
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
            IList<DocumentCategory> documentCategories = documentCategoryDao.GetAll();
            ViewBag.DocumentCategories = documentCategories;
            IList<GroupUser> groupUser = new GroupUserDao().GetGroupWhereUser(new UserDao().GetByLogin(User.Identity.Name));
            IList<Group> groups = new List<Group>();
            foreach (GroupUser groupUser1 in groupUser)
            {
                groups.Add(groupUser1.Group);
            }

            ViewBag.Groups = groups;
            return View();
        }

        [HttpPost]
        public ActionResult Add(Document document, HttpPostedFileBase image, int documentId, int? groupId)
        {
            try
            {
   if (ModelState.IsValid)
            {
                if (image != null)
                {
                    if (image.ContentType == "image/png" ||
                        image.ContentType == "image/jpg" || image.ContentType == "image/jpeg")
                    {
                        Image scan = Image.FromStream(image.InputStream);
                        Bitmap b = new Bitmap(scan);

                      
                        Guid guid = Guid.NewGuid();
                        string imageName = null;
                        if (image.ContentType == "image/png")
                        {
                            imageName = guid.ToString() + ".png";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Png);
                        }

                        if (image.ContentType == "image/jpg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }

                        if (image.ContentType == "image/jpeg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }
                        
                       
                        scan.Dispose();
                        b.Dispose();

                        document.Type = imageName;
                    }

                  
                }

                document.DateOfEstablishment = DateTime.Today.Date;
                document.DocumentCategory = new DocumentCategoryDao().GetById(documentId);
                document.UserId = new UserDao().GetByLogin(User.Identity.Name);
                if (groupId != null)
                {
                    document.Group = new GroupDao().GetById(groupId);
                }
                
                
                DocumentDao documentDao = new DocumentDao();
                documentDao.Create(document);
                IList<Notification> notifications = new List<Notification>();
                
                for (int i = 0; i < document.FrequencyOfPaymentPerYear ; i++)
                {
                    Notification notification = new Notification();
                    notification.User = new UserDao().GetByLogin(User.Identity.Name);
                    notification.DateOfPay = document.DueDate.AddMonths(i);
                    notification.Document = document;
                    notification.Note = document.Name;
                    notifications.Insert(i,notification);
                    
                }
                NotificationDao notificationDao = new NotificationDao();
                notificationDao.Create(notifications);
               
               
                
                
                
                TempData["message-success"] = "Dokument" + document.Name + "pridan";
            }
            else
            {
                
                return View("Create", document);
            }
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Document");
            }
         

            return RedirectToAction("Index");
        }

       
        public ActionResult Edit(int id)
        {
            DocumentCategoryDao documentCategoryDao = new DocumentCategoryDao();
            IList<DocumentCategory> documentCategories = documentCategoryDao.GetAll();
            ViewBag.DocumentCategories = documentCategories;

            DocumentDao documentDao = new DocumentDao();
            Document d = documentDao.GetById(id);
            return View(d);
        }

       
        public ActionResult Update(Document d, HttpPostedFileBase picture, int documentId, int? groupId)
        {
            try
            {
                if (picture != null)
                {
                    if (picture.ContentType == "image/png" ||
                        picture.ContentType == "image/jpg" || picture.ContentType == "image/jpeg")
                    {
                        Image scan = Image.FromStream(picture.InputStream);
                        Bitmap b = new Bitmap(scan);

                      
                        Guid guid = Guid.NewGuid();
                        string imageName = null;
                        if (picture.ContentType == "image/png")
                        {
                            imageName = guid.ToString() + ".png";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Png);
                        }

                        if (picture.ContentType == "image/jpg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }

                        if (picture.ContentType == "image/jpeg")
                        {
                            imageName = guid.ToString() + ".jpeg";
                            b.Save(Server.MapPath("~/uploads/invoices/" + imageName), ImageFormat.Jpeg);
                        }
                        
                       
                        scan.Dispose();
                        b.Dispose();

                        d.Type = imageName;
                    }

                 
                }
                 
                DocumentDao documentDao = new DocumentDao();
                d.DocumentCategory = new DocumentCategoryDao().GetById(documentId);
                d.UserId = new UserDao().GetByLogin(User.Identity.Name);
          
             
                
                if (groupId != null)
                {
                    d.Group = new GroupDao().GetById(groupId);
                }
                
            
                documentDao.Update(d);
                IList<Notification> notifications = new List<Notification>();
                NotificationDao notificationDao1= new NotificationDao();
                notificationDao1.Delete(d.Id);
                
                for (int i = 0; i < d.FrequencyOfPaymentPerYear ; i++)
                {
                    Notification notification = new Notification();
                    notification.User = new UserDao().GetByLogin(User.Identity.Name);
                    notification.DateOfPay = d.DueDate.AddMonths(i);
                    notification.Document = d;
                    notification.Note = d.Name;
                    notifications.Insert(i,notification);
                    
                }
                notificationDao1.Create(notifications);

                
                
                
                
                
                
                
                
                
                
                
                TempData["message-success"] = "Dokument " + d.Name + " byla upravena";
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Document");
            }

            return RedirectToAction("Index", "Document");
        }

        
        public ActionResult Delete(int id)
        {
            try
            {
                DocumentDao documentDao = new DocumentDao();
                Document document = documentDao.GetById(id);
                documentDao.DeleteNotificaitionFromDocuments(id);
                System.IO.File.Delete(Server.MapPath("~/uploads/invoices/" + document.Type));

                documentDao.Delete(document);

                TempData["message-success"] = "Dokument "+ document.Name +" byl úspěšně smazán";
            }
            catch (Exception e)
            {
                TempData["message-failed"] = "Něco se pokazilo :( ";
                return RedirectToAction("Index", "Document");
            }

            return RedirectToAction("Index");
        }
    }
}