using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using DataContainer.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DataContainer.Dao
{
    public class UserDao : DaoBase<User>
    {
        public UserDao() : base()
        {
            
        }

        public User GetByLoginAndPassword(string login, string password)
        {
            return 
            session.CreateCriteria<User>().Add(Restrictions.Eq("Login", login))
                .Add(Restrictions.Eq("Password", password))
                .UniqueResult<User>();
        }
        
        public User GetByLogin(string login)
        {
            return 
                session.CreateCriteria<User>().Add(Restrictions.Eq("Login", login))
                    .UniqueResult<User>();
        }

        public bool isLoginFree(string login)
        {
            if (session.CreateCriteria<User>().Add(Restrictions.Eq("Login", login)).UniqueResult<User>() != null)
            {
                return false;
            }

            return true;
        }
       
        public GroupUser AddUserMemberShipAfterGroupAdd(string login)
        {
            
            User user = session.CreateCriteria<User>().Add(Restrictions.Eq("Login", login))
                    .UniqueResult<User>();
            GroupUser gu = new GroupUser();
            gu.Member = Member.SetGroupLeaderRol();
            return gu;




        }
        
        
        public void DeleteDocumentsAfterDeletingUser(int id)
        {
            IList<Document> documents = session.CreateCriteria<Document>()
                .CreateAlias("UserId", "usDoc")
                .Add(Restrictions.Eq("usDoc.Id", id))
                .List<Document>();

            using (ITransaction tr = session.BeginTransaction())
            {
                foreach (Document d in documents)
                {
                    session.Delete(d);
                    
                }
                tr.Commit();
            }
        }
        
        public void RemoveGroupMembershipAfterDeletingUser(int id)
        {
            IList<GroupUser> documents = session.CreateCriteria<GroupUser>()
                .CreateAlias("User", "usDoc")
                .Add(Restrictions.Eq("usDoc.Id", id))
                .List<GroupUser>();

            using (ITransaction tr = session.BeginTransaction())
            {
                foreach (GroupUser groupUser in documents)
                {
                    session.Delete(groupUser);
                    
                }
                tr.Commit();
            }
        }
        
        public void RemoveCashFlowAfterDeletingUser(int id)
        {
            IList<CashFlow> cashFlows = session.CreateCriteria<CashFlow>()
                .CreateAlias("UserId", "usDoc")
                .Add(Restrictions.Eq("usDoc.Id", id))
                .List<CashFlow>();

            using (ITransaction tr = session.BeginTransaction())
            {
                foreach (CashFlow cashFlow in cashFlows)
                {
                    session.Delete(cashFlow);
                    
                }
                tr.Commit();
            }
        }
        
        public IList<User> GetUserPaged(int count, int pageNumber, out int totalDocuments)
        {
         
            
            totalDocuments = session.CreateCriteria<User>()
                .SetProjection(Projections.RowCount()).UniqueResult<int>();

            return session.CreateCriteria<User>()
                .AddOrder(Order.Asc("Login"))
                .SetFirstResult((pageNumber - 1) * count)
                .SetMaxResults(count)
                .List<User>();
        }
        
        

       
     
    }
}